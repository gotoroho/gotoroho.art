<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;

class PublishController extends AbstractController
{
    /**
     * @Route("publish", name="publish")
     */
    public function publish(Request $request, HubInterface $hub): Response
    {
        $json = json_decode($request->getContent(), true);

        $update = new Update(
            $this->generateUrl('home'),
            $json['message']
        );

        $hub->publish($update);

        return new Response();
    }
}
