import { Controller } from '@hotwired/stimulus';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * chat_room_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    static targets = ['mercureUrl', 'publishUrl', 'message', 'messages']

    connect() {
        const mercureUrl = this.getTarget('mercureUrl').value
        const eventSource = new EventSource(mercureUrl)

        eventSource.onmessage = event => {
            try {
                const p = document.createElement('p')

                p.textContent = event.data

                this.getTarget('messages').appendChild(p)

                console.log(this.getTarget('messages'))
            } catch (error) {
                console.log('error!')
                alert(error.message)
            } finally {
                console.log('message appended')
            }
        }
    }

    send() {
        const inputEl = this.getTarget('message')
        const message = inputEl.value

        fetch(this.getTarget('publishUrl').value, {
            method: 'POST',
            body: JSON.stringify({
                message
            })
        })
            .then(() => {
                inputEl.value = ''
            })
    }

    getTarget(targetName) {
        return this[`${targetName}Target`]
    }
}
