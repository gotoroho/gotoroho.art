DOCKER_COMPOSE := docker-compose -f ./deploy/docker-compose.yml -p gotorohoart

build:
	$(DOCKER_COMPOSE) build

up:
	$(DOCKER_COMPOSE) up -d

in:
	$(DOCKER_COMPOSE) exec php sh

composer-install:
	$(DOCKER_COMPOSE) exec -T php composer install
